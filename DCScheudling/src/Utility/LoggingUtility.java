package Utility;

import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

//created on 5/28/18 by David Coty
public class LoggingUtility {

    private static final Logger log = Logger.getLogger(LoggingUtility.class.getName());
    private static FileHandler logfilehandle = null;

    public static void initalizeLog() {

        try {
            logfilehandle = new FileHandler("DCSALog.%u.%g.txt", 1024 * 1024, 10, true);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }

        Logger logger = Logger.getLogger("");
        logfilehandle.setFormatter(new SimpleFormatter());
        logger.addHandler(logfilehandle);
        logger.setLevel(Level.INFO);
    }
}

