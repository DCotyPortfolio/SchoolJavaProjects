package controllers;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class IC_main extends Application {


    @Override
    public void start(Stage primaryStage) {
        try {
            Stage window = primaryStage;
            window.setTitle("Inventory Management System Demo");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/IC_main.fxml"));
            AnchorPane MainScreenView = loader.load();
            Scene scene = new Scene(MainScreenView);
            window.setScene(scene);
            window.show();

        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
