package models;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public abstract class IC_part {


    private final StringProperty name;
    private final IntegerProperty partID;
    private final DoubleProperty price;
    private final IntegerProperty inStock;
    private final IntegerProperty min;
    private final IntegerProperty max;

    IC_part() {
        partID = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        inStock = new SimpleIntegerProperty();
        price = new SimpleDoubleProperty();
        min = new SimpleIntegerProperty();
        max = new SimpleIntegerProperty();
    }

    public IntegerProperty partID() {
        return partID;
    }

    public StringProperty Name() {
        return name;
    }

    public DoubleProperty Price() {
        return price;
    }

    public IntegerProperty inStock() {
        return inStock;
    }

    public int getPartID() {
        return this.partID.get();
    }

    public String getName() {
        return this.name.get();
    }

    public double getPrice() {
        return this.price.get();
    }

    public int getInStock() {
        return this.inStock.get();
    }

    public int getMin() {
        return this.min.get();
    }

    public int getMax() {
        return this.max.get();
    }


    public void setPartID(int partID) {
        this.partID.set(partID);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public void setInStock(int inv) {
        this.inStock.set(inv);
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public void setMax(int max) {
        this.max.set(max);
    }


    public static String ValidatePart(String name, int min, int max, int invCount, double price, String errmsg) {
        if (name == null) {
            errmsg = errmsg + ("Name cannot be blank. ");
        }
        if (invCount < 1) {
            errmsg = errmsg + ("Inventory must be larger than 0. ");
        }
        if (price < 1) {
            errmsg = errmsg + ("Price must be larger than 0. ");
        }
        if (min > max) {
            errmsg = errmsg + ("The inventory minimum must be less than inventory maximum. ");
        }
        if (invCount < min || invCount > max) {
            errmsg = errmsg + ("Part inventory counts must be between the minimum and maximum values. ");
        }
        return errmsg;
    }

}

