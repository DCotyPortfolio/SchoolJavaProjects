package Utility;

import java.sql.Connection;
import java.sql.DriverManager;

//created on 5/28/18 by David Coty

public class DBConnectionUtility {

    private static Connection conn = null;


// private static String driver = "com.mysql.cj.jdbc.Driver";

// enter your connection information here

//    private static String driver =
//    private static String db =
//    private static String url =
//    private static String user =
//    private static String pass =


    public static void initialize() {

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException ce) {
            System.out.println("Class not found. please add the jdbc driver to config");
            ce.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static Connection getDBConnection() {
        return conn;
    }


    public static void closeDBConnection() {
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Connection closed.");
        }
    }

}
