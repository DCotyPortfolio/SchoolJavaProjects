package Controller;

import Model.AppointmentModel;
import Model.CustomerModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import Utility.LoggingUtility;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


//created on 6/4/18 by David Coty
public class LoginPageController {
    //form elements

    @FXML
    private Label loginStatusMessageText;
    @FXML
    private TextField loginUsernameField;
    @FXML
    private PasswordField loginPasswordField;
    @FXML
    private Text loginUsernameText;
    @FXML
    private Text loginPasswordText;
    @FXML
    private Text loginTitleText;
    @FXML
    private Button loginSigninButton;
    @FXML
    private Button loginCancelButton;


    // constructor
    public LoginPageController() {
    }

    ResourceBundle rb = ResourceBundle.getBundle("login", Locale.getDefault());
    private MainController mainApp;
    private final ZoneId newzid = ZoneId.systemDefault();
    private final DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    UserModel user = new UserModel();
    ObservableList<AppointmentModel> reminderList;
    private final static Logger LOGGER = Logger.getLogger(LoggingUtility.class.getName());


    @FXML
    void signin(ActionEvent event) {
        String userN = loginUsernameField.getText();   // Collecting the input
        String pass = loginPasswordField.getText(); // Collecting the input

        if (userN.length() == 0 || pass.length() == 0)  // Checking for empty field
            loginStatusMessageText.setText(rb.getString("empty"));
        else {

            UserModel validUser = validateLogin(userN, pass);
            if (validUser == null) {
                loginStatusMessageText.setText(rb.getString("incorrect"));
                return;
            }
            populateReminderList();
            reminder();
            mainApp.showAppMenu(validUser);
            mainApp.showAppointmentScreen(validUser);
            LOGGER.log(Level.INFO, "{0}  logged in successfully", validUser.getUsername());


        }
    }


    UserModel validateLogin(String username, String password) {
        try {
            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement("SELECT * FROM user WHERE userName=? AND password=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                user.setUsername(rs.getString("userName"));
                user.setPassword(rs.getString("password"));
                user.setID(rs.getInt("userId"));
            } else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }


    @FXML
    void cancel(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Cancel");
        alert.setHeaderText("Close Program?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent((ButtonType response) -> {
                            Platform.exit();
                            System.exit(0);
                        }
                );
    }


    public void setLogin(MainController mainApp) {
        this.mainApp = mainApp;
        reminderList = FXCollections.observableArrayList();
        loginTitleText.setText(rb.getString("title"));
        loginUsernameText.setText(rb.getString("username"));
        loginPasswordText.setText(rb.getString("password"));
        loginSigninButton.setText(rb.getString("signin"));
        loginCancelButton.setText(rb.getString("cancel"));
    }


    private void populateReminderList() {
        System.out.println(user.getUsername());
        try {
            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT appointment.appointmentId, appointment.customerId, appointment.title, appointment.description, "
                            + "appointment.`start`, appointment.`end`, customer.customerId, customer.customerName, appointment.createdBy "
                            + "FROM appointment, customer "
                            + "WHERE appointment.customerId = customer.customerId AND appointment.createdBy = ? "
                            + "ORDER BY `start`");
            pst.setString(1, user.getUsername());
            ResultSet rs = pst.executeQuery();


            while (rs.next()) {
                String tAppointmentId = rs.getString("appointment.appointmentId");
                Timestamp tsStart = rs.getTimestamp("appointment.start");
                ZonedDateTime newzdtStart = tsStart.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalStart = newzdtStart.withZoneSameInstant(newzid);
                Timestamp tsEnd = rs.getTimestamp("appointment.end");
                ZonedDateTime newzdtEnd = tsEnd.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalEnd = newzdtEnd.withZoneSameInstant(newzid);
                String tTitle = rs.getString("appointment.title");
                String tType = rs.getString("appointment.description");
                CustomerModel tCustomer = new CustomerModel(rs.getString("appointment.customerId"), rs.getString("customer.customerName"));
                String tUser = rs.getString("appointment.createdBy");
                reminderList.add(new AppointmentModel(tAppointmentId, newLocalStart.format(timeDTF), newLocalEnd.format(timeDTF), tTitle, tType, tCustomer, tUser));

            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void reminder() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowPlus15Min = now.plusMinutes(15);

        FilteredList<AppointmentModel> filteredData = new FilteredList<>(reminderList);

        filteredData.setPredicate(row -> {
                    LocalDateTime rowDate = LocalDateTime.parse(row.getStart(), timeDTF);
                    return rowDate.isAfter(now.minusMinutes(1)) && rowDate.isBefore(nowPlus15Min);
                }
        );
        if (filteredData.isEmpty()) {
            System.out.println(" No Appointment Reminders.");

        } else {
            String type = filteredData.get(0).getDescription();
            String customer = filteredData.get(0).getCustomer().getCustomerName();
            String start = filteredData.get(0).getStart();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment Reminder");
            alert.setHeaderText("Upcoming Appointment Reminder.");
            alert.setContentText("You have an upcoming  " + type + " appointment with " + customer +
                    " set for " + start + ".");
            alert.showAndWait();
        }


    }


}
