package controllers;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.IC_inHousePart;
import models.IC_inventory;
import models.IC_outSourcedPart;
import models.IC_part;

//created on 3/18/18 by David Coty
public class IC_modifyPartController implements Initializable{

    private boolean isOutsource;
    private final int indexpart = IC_mainController.partToModifyIndex();
    private String errmsg = "";
    private int partID;

    @FXML
    private TextField PartIDField;
    @FXML
    private TextField PartNameField;
    @FXML
    private TextField PartInvField;
    @FXML
    private TextField PartPriceField;
    @FXML
    private TextField PartMinField;
    @FXML
    private TextField MachineIDField;
    @FXML
    private TextField PartMaxField;
    @FXML
    private Label MachineIDLabel;
    @FXML
    private RadioButton InHousePartRadioButton;
    @FXML
    private RadioButton OutsourcePartRadioButton;

    @FXML
    void OutsourcedRadioBtnChecked(ActionEvent event) {
        isOutsource = true;
        OutsourcePartRadioButton.setSelected(true);
        InHousePartRadioButton.setSelected(false);
        MachineIDLabel.setText("Company Name");
    }

    @FXML
    void InHouseRadioBtnChecked(ActionEvent event) {
        isOutsource = false;
        InHousePartRadioButton.setSelected(true);
        OutsourcePartRadioButton.setSelected(false);
        MachineIDLabel.setText("Machine ID");
    }

    @FXML
    void PartCancelClicked(ActionEvent event) {

        Alert cancelAlert = new Alert(Alert.AlertType.CONFIRMATION);
        cancelAlert.initModality(Modality.NONE);
        cancelAlert.setTitle("Cancel");
        cancelAlert.setHeaderText("Cancel");
        cancelAlert.setContentText("Cancel and go to main screen?");

        cancelAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                Parent Cancel = null;
                try {
                    Cancel = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Scene scene = new Scene(Cancel);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        });
    }

    @FXML
    void PartSaveClicked(ActionEvent event) throws IOException {
        String partName = PartNameField.getText();
        String partInv = PartInvField.getText();
        String partPrice = PartPriceField.getText();
        String partMin = PartMinField.getText();
        String partMax = PartMaxField.getText();
        String partDyn = MachineIDField.getText();
        try {
            errmsg = IC_part.ValidatePart(partName, Integer.parseInt(partMin), Integer.parseInt(partMax), Integer.parseInt(partInv), Double.parseDouble(partPrice), errmsg);
            if (errmsg.length() > 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error Adding Part");
                alert.setHeaderText("Error!");
                alert.setContentText(errmsg);
                alert.showAndWait();
            } else {
                if (InHousePartRadioButton.isSelected()) {
                    IC_inHousePart IHPart = new IC_inHousePart();
                    IHPart.setPartID(partID);
                    IHPart.setName(partName);
                    IHPart.setPrice(Double.parseDouble(partPrice));
                    IHPart.setInStock(Integer.parseInt(partInv));
                    IHPart.setMin(Integer.parseInt(partMin));
                    IHPart.setMax(Integer.parseInt(partMax));
                    IHPart.setMachineID(Integer.parseInt(partDyn));
                    IC_inventory.updatePart(indexpart, IHPart);
                } else {
                    IC_outSourcedPart outsourcedPart = new IC_outSourcedPart();
                    outsourcedPart.setPartID(partID);
                    outsourcedPart.setName(partName);
                    outsourcedPart.setPrice(Double.parseDouble(partPrice));
                    outsourcedPart.setInStock(Integer.parseInt(partInv));
                    outsourcedPart.setMin(Integer.parseInt(partMin));
                    outsourcedPart.setMax(Integer.parseInt(partMax));
                    outsourcedPart.setCompanyName(partDyn);
                    IC_inventory.updatePart(indexpart, outsourcedPart);
                }

                Parent modifyProductCancel = FXMLLoader.load(getClass().getResource("/views/IC_Main.fxml"));
                Scene scene = new Scene(modifyProductCancel);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        } catch (NumberFormatException e) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Adding Part!");
            alert.setHeaderText("Error");
            alert.setContentText("Blank Fields are not allowed. ");
            alert.showAndWait();
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        IC_part part = IC_inventory.allParts().get(indexpart);
        partID = IC_inventory.allParts().get(indexpart).getPartID();
        PartIDField.setText(String.valueOf(partID));
        PartNameField.setText(part.getName());
        PartInvField.setText(Integer.toString(part.getInStock()));
        PartPriceField.setText(Double.toString(part.getPrice()));
        PartMinField.setText(Integer.toString(part.getMin()));
        PartMaxField.setText(Integer.toString(part.getMax()));
        if (part instanceof IC_inHousePart) {
            MachineIDField.setText(Integer.toString(((IC_inHousePart) IC_inventory.allParts().get(indexpart)).getMachineID()));
            MachineIDLabel.setText("Machine ID");
            InHousePartRadioButton.setSelected(true);

        } else {
            MachineIDField.setText(((IC_outSourcedPart) IC_inventory.allParts().get(indexpart)).getCompanyName());
            MachineIDLabel.setText("Company Name");
            OutsourcePartRadioButton.setSelected(true);
        }
    }





}
