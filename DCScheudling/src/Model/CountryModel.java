package Model;

//created on 6/4/18 by David Coty
public class CountryModel {

    private Integer countryId;
    private String country;

    //Constructors
    public CountryModel() {
    }

    public CountryModel(Integer countryId) {
        this.countryId = countryId;
    }

    public CountryModel(Integer countryId, String country) {
        this.countryId = countryId;
        this.country = country;
    }

    //getters and setters
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countryId != null ? countryId.hashCode() : 0);
        return hash;


    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CountryModel)) {
            return false;
        }
        CountryModel other = (CountryModel) object;
        if ((this.countryId == null && other.countryId != null) || (this.countryId != null && !this.countryId.equals(other.countryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Country[ countryId=" + countryId + " ]";
    }

}