package Model;

//created on 6/4/18 by David Coty
public class ReportModel {

    private String month;
    private String amount;
    private String type;

    //constructor
    public ReportModel(String month, String amount, String type) {
        this.month = month;
        this.amount = amount;
        this.type = type;
    }

    //getters and setters
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return "ReportModel{" +
                "month='" + month + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
