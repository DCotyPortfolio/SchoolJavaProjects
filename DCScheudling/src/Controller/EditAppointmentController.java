package Controller;

import Model.AppointmentModel;
import Model.CustomerModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EditAppointmentController {


    @FXML
    private Label apptLabel;

    @FXML
    private TextField titleField;

    @FXML
    private ComboBox<String> startComboBox;

    @FXML
    private ComboBox<String> endComboBox;

    @FXML
    private DatePicker datePicker;

    @FXML
    private ComboBox<String> typeComboBox;

    @FXML
    private Button apptSaveButton;

    @FXML
    private Button apptCancelButton;

    @FXML
    private TableView<CustomerModel> customerSelectTableView;

    @FXML
    private TableColumn<CustomerModel, String> customerNameApptColumn;

    @FXML
    private TextField customerSearchField;


    private Stage dialogStage;
    private MainController mainApp;
    private boolean okClicked = false;
    private final ZoneId zid = ZoneId.systemDefault();
    private AppointmentModel selectedAppt;
    private UserModel currentUser;

    private ObservableList<CustomerModel> masterData = FXCollections.observableArrayList();
    private final ObservableList<String> startTimes = FXCollections.observableArrayList();
    private final ObservableList<String> endTimes = FXCollections.observableArrayList();
    private final DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    private final DateTimeFormatter dateDTF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    ObservableList<AppointmentModel> apptTimeList;

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void save(ActionEvent event) {
        if (validateAppointment()) {
            if (isOkClicked()) {
                updateAppt();
            } else {
                saveAppt();
            }
            dialogStage.close();
        }

    }

    @FXML
    private void cancel(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Cancel");
        alert.setHeaderText("Do you wish to Cancel?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> dialogStage.close());

    }

    public void setDialogStage(Stage dialogStage, UserModel currentUser) {
        this.dialogStage = dialogStage;
        this.currentUser = currentUser;

        populateTypeList();
        customerNameApptColumn.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        masterData = populateCustomerList();


        FilteredList<CustomerModel> filteredData = new FilteredList<>(masterData, p -> true);


        customerSearchField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(customer -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }


                String lowerCaseFilter = newValue.toLowerCase();

                if (customer.getCustomerName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<CustomerModel> sortedData = new SortedList<>(filteredData);


        sortedData.comparatorProperty().bind(customerSelectTableView.comparatorProperty());


        customerSelectTableView.setItems(sortedData);
        LocalTime time = LocalTime.of(8, 0);
        do {
            startTimes.add(time.format(timeDTF));
            endTimes.add(time.format(timeDTF));
            time = time.plusMinutes(15);
        } while (!time.equals(LocalTime.of(17, 15)));
        startTimes.remove(startTimes.size() - 1);
        endTimes.remove(0);

        datePicker.setValue(LocalDate.now());

        startComboBox.setItems(startTimes);
        endComboBox.setItems(endTimes);
        startComboBox.getSelectionModel().select(LocalTime.of(8, 0).format(timeDTF));
        endComboBox.getSelectionModel().select(LocalTime.of(8, 15).format(timeDTF));

    }

    public void setAppointment(AppointmentModel appointment) {


        okClicked = true;
        selectedAppt = appointment;

        String start = appointment.getStart();

        LocalDateTime startLDT = LocalDateTime.parse(start, dateDTF);
        String end = appointment.getEnd();
        LocalDateTime endLDT = LocalDateTime.parse(end, dateDTF);

        apptLabel.setText("Edit Appointment");
        titleField.setText(appointment.getTitle());
        typeComboBox.setValue(appointment.getDescription());
        customerSelectTableView.getSelectionModel().select(appointment.getCustomer());
        datePicker.setValue(LocalDate.parse(appointment.getStart(), dateDTF));
        startComboBox.getSelectionModel().select(startLDT.toLocalTime().format(timeDTF));
        endComboBox.getSelectionModel().select(endLDT.toLocalTime().format(timeDTF));


    }

    private void saveAppt() {

        LocalDate localDate = datePicker.getValue();
        LocalTime startTime = LocalTime.parse(startComboBox.getSelectionModel().getSelectedItem(), timeDTF);
        LocalTime endTime = LocalTime.parse(endComboBox.getSelectionModel().getSelectedItem(), timeDTF);

        LocalDateTime startDT = LocalDateTime.of(localDate, startTime);
        LocalDateTime endDT = LocalDateTime.of(localDate, endTime);

        ZonedDateTime startUTC = startDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));
        ZonedDateTime endUTC = endDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));

        Timestamp startsqlts = Timestamp.valueOf(startUTC.toLocalDateTime());
        Timestamp endsqlts = Timestamp.valueOf(endUTC.toLocalDateTime());

        try {

            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement("INSERT INTO appointment "
                    + "(customerId, title, description, location, contact, url, start, end, createDate, createdBy, lastUpdate, lastUpdateBy)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, ?)");

            pst.setString(1, customerSelectTableView.getSelectionModel().getSelectedItem().getCustomerId());
            pst.setString(2, titleField.getText());
            pst.setString(3, typeComboBox.getValue());
            pst.setString(4, "");
            pst.setString(5, "");
            pst.setString(6, "");
            pst.setTimestamp(7, startsqlts);
            pst.setTimestamp(8, endsqlts);
            pst.setString(9, currentUser.getUsername());
            pst.setString(10, currentUser.getUsername());
            int result = pst.executeUpdate();

            if (result == 1) {
                System.out.println("Appointment created");
            } else {
                System.out.println("Appointment not created");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private void updateAppt() {

        LocalDate localDate = datePicker.getValue();
        LocalTime startTime = LocalTime.parse(startComboBox.getSelectionModel().getSelectedItem(), timeDTF);
        LocalTime endTime = LocalTime.parse(endComboBox.getSelectionModel().getSelectedItem(), timeDTF);

        LocalDateTime startDT = LocalDateTime.of(localDate, startTime);
        LocalDateTime endDT = LocalDateTime.of(localDate, endTime);

        ZonedDateTime startUTC = startDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));
        ZonedDateTime endUTC = endDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));

        Timestamp startsqlts = Timestamp.valueOf(startUTC.toLocalDateTime());
        Timestamp endsqlts = Timestamp.valueOf(endUTC.toLocalDateTime());

        try {

            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement("UPDATE appointment "
                    + "SET customerId = ?, title = ?, description = ?, start = ?, end = ?, lastUpdate = CURRENT_TIMESTAMP, lastUpdateBy = ? "
                    + "WHERE appointmentId = ?");

            pst.setString(1, customerSelectTableView.getSelectionModel().getSelectedItem().getCustomerId());
            pst.setString(2, titleField.getText());
            pst.setString(3, typeComboBox.getValue());
            pst.setTimestamp(4, startsqlts);
            pst.setTimestamp(5, endsqlts);
            pst.setString(6, currentUser.getUsername());
            pst.setString(7, selectedAppt.getAppointmentId());
            int result = pst.executeUpdate();
            if (result == 1) {
                System.out.println("Appointment Saved");
            } else {
                System.out.println("Appointment not Saved");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void populateTypeList() {
        ObservableList<String> typeList = FXCollections.observableArrayList();
        typeList.addAll("Consultation", "New Account", "Follow Up", "Close Account");
        typeComboBox.setItems(typeList);
    }

    protected ObservableList<CustomerModel> populateCustomerList() {

        String tCustomerId;
        String tCustomerName;

        ObservableList<CustomerModel> customerList = FXCollections.observableArrayList();
        try (


                PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement(
                        "SELECT customer.customerId, customer.customerName " +
                                "FROM customer, address, city, country " +
                                "WHERE customer.addressId = address.addressId AND address.cityId = city.cityId AND city.countryId = country.countryId");
                ResultSet rs = statement.executeQuery();) {


            while (rs.next()) {
                tCustomerId = rs.getString("customer.customerId");

                tCustomerName = rs.getString("customer.customerName");

                customerList.add(new CustomerModel(tCustomerId, tCustomerName));

            }

        } catch (SQLException sqe) {

            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return customerList;


    }

    private boolean validateAppointment() {
        String title = titleField.getText();
        String type = typeComboBox.getValue();
        CustomerModel customer = customerSelectTableView.getSelectionModel().getSelectedItem();
        LocalDate localDate = datePicker.getValue();
        LocalTime startTime = LocalTime.parse(startComboBox.getSelectionModel().getSelectedItem(), timeDTF);
        LocalTime endTime = LocalTime.parse(endComboBox.getSelectionModel().getSelectedItem(), timeDTF);

        LocalDateTime startDT = LocalDateTime.of(localDate, startTime);
        LocalDateTime endDT = LocalDateTime.of(localDate, endTime);

        ZonedDateTime startUTC = startDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));
        ZonedDateTime endUTC = endDT.atZone(zid).withZoneSameInstant(ZoneId.of("UTC"));

        String errorMessage = "";

        if (title == null || title.length() == 0) {
            errorMessage += "Enter an Appointment title.\n";
        }
        if (type == null || type.length() == 0) {
            errorMessage += "Select an Appointment type.\n";
        }
        if (customer == null) {
            errorMessage += "Select a Customer.\n";
        }
        if (startUTC == null) {
            errorMessage += "Select a Start time";
        }
        if (endUTC == null) {
            errorMessage += "Select an End time.\n";

        } else if (endUTC.equals(startUTC) || endUTC.isBefore(startUTC)) {
            errorMessage += "End time must be after Start time.\n";
        } else try {

            if (hasApptConflict(startUTC, endUTC)) {
                errorMessage += "Appointment Conflicts with another appointment. Select a new time.\n";
            }
        } catch (SQLException ex) {
            Logger.getLogger(EditAppointmentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (errorMessage.length() == 0) {
            return true;
        } else {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Invalid Appointment Fields.");
            alert.setContentText(errorMessage);
            alert.showAndWait();

            return false;
        }


    }

    private boolean hasApptConflict(ZonedDateTime newStart, ZonedDateTime newEnd) throws SQLException {
        String apptID;
        String consultant;
        if (isOkClicked()) {
            apptID = selectedAppt.getAppointmentId();
            consultant = selectedAppt.getUser();
        } else {
            apptID = "0";
            consultant = currentUser.getUsername();
        }
        System.out.println("ApptID: " + apptID);

        try {

            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT * FROM appointment "
                            + "WHERE (? BETWEEN start AND end OR ? BETWEEN start AND end OR ? < start AND ? > end) "
                            + "AND (appointmentID != ?)");
            pst.setTimestamp(1, Timestamp.valueOf(newStart.toLocalDateTime()));
            pst.setTimestamp(2, Timestamp.valueOf(newEnd.toLocalDateTime()));
            pst.setTimestamp(3, Timestamp.valueOf(newStart.toLocalDateTime()));
            pst.setTimestamp(4, Timestamp.valueOf(newEnd.toLocalDateTime()));
            // commenting out the consultant field, per feedback
            //pst.setString(5, consultant);
            pst.setString(5, apptID);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}

