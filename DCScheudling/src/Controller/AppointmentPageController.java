package Controller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import Model.AppointmentModel;
import Model.CustomerModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;



public class AppointmentPageController {


    @FXML
    private TableView<AppointmentModel> apptTableView;

    @FXML
    private TableColumn<AppointmentModel, ZonedDateTime> startApptColumn;

    @FXML
    private TableColumn<AppointmentModel, LocalDateTime> endApptColumn;

    @FXML
    private TableColumn<AppointmentModel, String> titleApptColumn;

    @FXML
    private TableColumn<AppointmentModel, String> typeApptColumn;

    @FXML
    private TableColumn<AppointmentModel, CustomerModel> customerApptColumn;

    @FXML
    private TableColumn<AppointmentModel, String> consultantApptColumn;

    @FXML
    private RadioButton weekRadioButton;

    @FXML
    private RadioButton monthRadioButton;

    @FXML
    private ToggleGroup apptToggleGroup;

    private MainController mainApp;
    private UserModel currentUser;
    private final DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    private final ZoneId newzid = ZoneId.systemDefault();
    ObservableList<AppointmentModel> apptList;


    public void setAppointmentScreen(MainController mainApp, UserModel currentUser) {
        this.mainApp = mainApp;
        this.currentUser = currentUser;
// TODO: 6/9/18 turn this from a radio button toggle group to a drop down list, or buttons
        apptToggleGroup = new ToggleGroup();
        this.weekRadioButton.setToggleGroup(apptToggleGroup);
        this.monthRadioButton.setToggleGroup(apptToggleGroup);
        //apptToggleGroup.selectToggle(this.weekRadioButton);

        startApptColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
        endApptColumn.setCellValueFactory(new PropertyValueFactory<>("end"));
        titleApptColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        typeApptColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        customerApptColumn.setCellValueFactory(new PropertyValueFactory<>("customer"));
        consultantApptColumn.setCellValueFactory(new PropertyValueFactory<>("user"));

        apptList = FXCollections.observableArrayList();
        populateAppointmentList();
        apptTableView.getItems().setAll(apptList);
    }


    @FXML
    void handleApptMonth(ActionEvent event) {

        LocalDate now = LocalDate.now();
        LocalDate nowPlus1Month = now.plusMonths(1);

        FilteredList<AppointmentModel> filteredData = new FilteredList<>(apptList);

        filteredData.setPredicate(row -> {

            LocalDate rowDate = LocalDate.parse(row.getStart(), timeDTF);

            return rowDate.isAfter(now.minusDays(1)) && rowDate.isBefore(nowPlus1Month);
        });
        apptTableView.setItems(filteredData);

    }


    @FXML
    void handleApptWeek(ActionEvent event) {

        LocalDate now = LocalDate.now();
        LocalDate nowPlus1Week = now.plusWeeks(1);
        FilteredList<AppointmentModel> filteredData = new FilteredList<>(apptList);


        filteredData.setPredicate(row -> {
            LocalDate rowDate = LocalDate.parse(row.getStart(), timeDTF);
            return rowDate.isBefore(nowPlus1Week);

        });

        apptTableView.setItems(filteredData);

    }


    @FXML
    void handleDeleteAppt(ActionEvent event) {
        AppointmentModel selectedAppointment = apptTableView.getSelectionModel().getSelectedItem();

        if (selectedAppointment != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm Deletion");
            alert.setHeaderText("Are you sure you want to delete " + selectedAppointment.getTitle() + " scheduled for " + selectedAppointment.getStart() + "?");
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> {
                                deleteAppointment(selectedAppointment);
                                mainApp.showAppointmentScreen(currentUser);
                            }
                    );
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Appointment selected for Deletion");
            alert.setContentText("Please select an Appointment in the Table to delete");
            alert.showAndWait();
        }

    }

    @FXML
    void handleEditAppt(ActionEvent event) {
        AppointmentModel selectedAppointment = apptTableView.getSelectionModel().getSelectedItem();

        if (selectedAppointment != null) {
            boolean okClicked = mainApp.showEditApptScreen(selectedAppointment, currentUser);
            mainApp.showAppointmentScreen(currentUser);

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Appointment selected");
            alert.setContentText("Please select an Appointment in the Table");
            alert.showAndWait();
        }

    }

    @FXML
    void handleNewAppt(ActionEvent event) throws IOException {
        boolean okClicked = mainApp.showNewApptScreen(currentUser);
        mainApp.showAppointmentScreen(currentUser);
    }

    private void populateAppointmentList() {

        try {

            PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT appointment.appointmentId, appointment.customerId, appointment.title, appointment.description, "
                            + "appointment.`start`, appointment.`end`, customer.customerId, customer.customerName, appointment.createdBy "
                            + "FROM appointment, customer "
                            + "WHERE appointment.customerId = customer.customerId "
                            + "ORDER BY `start`");
            ResultSet rs = statement.executeQuery();


            while (rs.next()) {

                String tAppointmentId = rs.getString("appointment.appointmentId");
                Timestamp tsStart = rs.getTimestamp("appointment.start");
                ZonedDateTime newzdtStart = tsStart.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalStart = newzdtStart.withZoneSameInstant(newzid);

                Timestamp tsEnd = rs.getTimestamp("appointment.end");
                ZonedDateTime newzdtEnd = tsEnd.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalEnd = newzdtEnd.withZoneSameInstant(newzid);

                String tTitle = rs.getString("appointment.title");

                String tType = rs.getString("appointment.description");

                CustomerModel tCustomer = new CustomerModel(rs.getString("appointment.customerId"), rs.getString("customer.customerName"));

                String tUser = rs.getString("appointment.createdBy");

                apptList.add(new AppointmentModel(tAppointmentId, newLocalStart.format(timeDTF), newLocalEnd.format(timeDTF), tTitle, tType, tCustomer, tUser));


            }

        } catch (SQLException sqe) {
            System.out.println("SQL Invalid");
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void deleteAppointment(AppointmentModel appointment) {
        try {
            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement("DELETE appointment.* FROM appointment WHERE appointment.appointmentId = ?");
            pst.setString(1, appointment.getAppointmentId());
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
