package models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class IC_inventory {
    private static final ObservableList<IC_product> products = FXCollections.observableArrayList();
    private static final ObservableList<IC_part> parts = FXCollections.observableArrayList();
    private static int partCount = 0;
    private static int productCount = 0;

    public static ObservableList<IC_part> allParts() {
        return parts;
    }

    public static void addPart(IC_part part) {
        parts.add(part);
    }

    public static void deletePart(IC_part part) {
        parts.remove(part);
    }

    public static void updatePart(int index, IC_part part) {
        parts.set(index, part);
    }

    public static int getPartCount() {
        partCount++;
        return partCount;
    }

    public static boolean validatePartDelete(IC_part part) {
        boolean isFound = false;

        for (IC_product product : products)
            if (product.getParts().contains(part)) {
                isFound = true;
            }
        return isFound;
    }

    public static int lookupPart(String searchTerm) {
        boolean isFound = false;
        int index = 0;
        if (isInteger(searchTerm)) {
            for (int i = 0; i < parts.size(); i++) {
                if (Integer.parseInt(searchTerm) == parts.get(i).getPartID()) {
                    index = i;
                    isFound = true;
                }
            }
        } else {
            for (int i = 0; i < parts.size(); i++) {
                if (searchTerm.equals(parts.get(i).getName())) {
                    index = i;
                    isFound = true;
                }
            }
        }
        if (isFound = true) {
            return index;
        } else {
            return -1;
        }
    }


    public static ObservableList<IC_product> products() {
        return products;
    }

    public static void addProduct(IC_product product) {
            products.add(product);
        }

    public static void removeProduct(IC_product product) {
        if (product.getParts().isEmpty()) {
            products.remove(product);
        }

 else {
            //need to alert user that the product contains parts and cannot be deleted.
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText("This Product contains parts and cannot be deleted. ");
            alert.showAndWait();
        }
    }

    public static int getProductCount() {
        productCount++;
        return productCount;
    }

    public static int lookupProduct(String searchTerm) {
        boolean isFound = false;
        int index = 0;

        if (isInteger(searchTerm)) {
            for (int i = 0; i < products.size(); i++) {
                if (Integer.parseInt(searchTerm) == products.get(i).getProductID()) {
                    index = i;
                    isFound = true;
                }
            }
        } else {
            for (int i = 0; i < products.size(); i++) {
                if (searchTerm.equals(products.get(i).getName())) {
                    index = i;
                    isFound = true;
                }
            }
        }
        if (isFound = true) {
            return index;
        } else {
            return -1;
        }
    }

    public static void updateProduct(int index, IC_product product) {
        products.set(index, product);
    }

    private static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

