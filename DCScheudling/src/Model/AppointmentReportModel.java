package Model;

//created on 6/4/18 by David Coty
public class AppointmentReportModel {

    private String month;
    private String amount;
    private String type;

    //constructor

    public AppointmentReportModel(String month, String type, String amount) {
        this.month = month;
        this.type = type;
        this.amount = amount;
    }

    //getters and setters
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    @Override
    public String toString() {
        return "AppointmentReportModel{" +
                "month='" + month + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
