package Model;

//created on 5/28/18 by David Coty

public class CityModel {
    private int cityID;
    private String city;
    private int countryID;

    //Constructors
    public CityModel(int cityID) {
        this.cityID = cityID;
    }

    public CityModel(int cityID, String city) {
        this.cityID = cityID;
        this.city = city;
    }

    public CityModel(int cityID, String city, int countryID) {
        this.cityID = cityID;
        this.city = city;
        this.countryID = countryID;
    }

    //getters
    public int getCityID() {
        return cityID;
    }

    public String getCity() {
        return city;
    }

    public int getCountryID() {
        return countryID;
    }

    //setters
    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    //overrides
    @Override
    public String toString() {
        return city;
    }
}
