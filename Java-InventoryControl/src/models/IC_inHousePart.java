package models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

//created on 3/18/18 by David Coty
public class IC_inHousePart extends IC_part {

   private final IntegerProperty machineID;


    public IC_inHousePart() {
        super();
       machineID = new SimpleIntegerProperty();
    }

    public void setMachineID(int machineID) {
        this.machineID.set(machineID);
    }

    public int getMachineID() {
        return this.machineID.get();
    }
}
