package Model;

//created on 6/4/18 by David Coty
public class AppointmentModel {

    private String appointmentId;
    private CustomerModel customer;
    private String title;
    private String description;
    private String start;
    private String end;
    private String user;

    //constructors

    public AppointmentModel() {
    }

    public AppointmentModel(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public AppointmentModel(String appointmentId, String start, String end, String title, String description, CustomerModel customer, String user) {
        this.appointmentId = appointmentId;
        this.start = start;
        this.end = end;
        this.title = title;
        this.description = description;
        this.customer = customer;
        this.user = user;
    }

    public AppointmentModel(String start, String end, String user) {
        this.start = start;
        this.end = end;
        this.user = user;
    }

    // getters and setters

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


    @Override
    public String toString() {
        return "AppointmentModel{" +
                "appointmentId='" + appointmentId + '\'' +
                ", customer=" + customer +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
