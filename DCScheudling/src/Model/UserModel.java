package Model;

//created on 5/28/18 by David Coty
public class UserModel {
    private int ID;
    private String username;
    private String password;

    //constructors
    public UserModel(int ID, String username, String password) {
        this.ID = ID;
        this.username = username;
        this.password = password;
    }

    public UserModel() {
    }

    //getters and setters
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
