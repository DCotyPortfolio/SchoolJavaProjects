package Model;

//created on 5/28/18 by David Coty
public class AddressModel {

    private Integer addressID;
    private String address;
    private String address2;
    private int cityID;
    private String postalCode;
    private String phone;

    // constructors
    public AddressModel(Integer addressID, String address, String address2, int cityID, String postalCode, String phone) {

        this.addressID = addressID;
        this.address = address;
        this.address2 = address2;
        this.cityID = cityID;
        this.postalCode = postalCode;
        this.phone = phone;
    }

    public AddressModel(Integer addressID) {
        this.addressID = addressID;
    }

    public AddressModel() {

    }

    //getters and setters
    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressID != null ? addressID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AddressModel)) {
            return false;
        }
        AddressModel other = (AddressModel) object;
        if ((this.addressID == null && other.addressID != null) || (this.addressID != null && !this.addressID.equals(other.addressID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.AddressModel[ addressID=" + addressID + " ]";

    }
}


  
