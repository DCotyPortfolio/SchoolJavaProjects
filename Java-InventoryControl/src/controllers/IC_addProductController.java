package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.IC_inventory;
import models.IC_part;
import models.IC_product;

//created on 3/18/18 by David Coty
public class IC_addProductController implements Initializable {

    private final ObservableList<IC_part> currentParts = FXCollections.observableArrayList();
    private String errmsg = "";
    private int productID;

    @FXML
    private TextField IDField;
    @FXML
    private TextField NameField;
    @FXML
    private TextField PriceField;
    @FXML
    private TextField InvCountField;
    @FXML
    private TextField MinField;
    @FXML
    private TextField MaxField;
    @FXML
    private TextField DelPartSearchField;
    @FXML
    private TextField AddPartSearchField;
    @FXML
    private TableView<IC_part> AddTableView;
    @FXML
    private TableColumn<IC_part, Integer> PartIDCol;
    @FXML
    private TableColumn<IC_part, String> PartNameCol;
    @FXML
    private TableColumn<IC_part, Integer> InvLevelCol;
    @FXML
    private TableColumn<IC_part, Double> PriceCol;
    @FXML
    private TableView<IC_part> DeleteTableView;
    @FXML
    private TableColumn<IC_part, Integer> CurrentPartIDCol;
    @FXML
    private TableColumn<IC_part, String> CurrentPartNameCol;
    @FXML
    private TableColumn<IC_part, Integer> CurrentInvCol;
    @FXML
    private TableColumn<IC_part, Double> CurrentPriceCol;

    public IC_addProductController() {
    }

    @FXML void ClearAdd(ActionEvent event) {
        updatePartTable();
        AddPartSearchField.setText("");
    }

    @FXML void ClearRemove(ActionEvent event) {
        updateCurrentPartTable();
        DelPartSearchField.setText("");
    }

    @FXML
    void SearchPartAddBtn(ActionEvent event) {

        String searchPart = AddPartSearchField.getText();
        int index;

        if (IC_inventory.lookupPart(searchPart) == -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Part not found");
            alert.setHeaderText("Part not found");
            alert.setContentText("Part not found");
            alert.showAndWait();
        } else {
            index = IC_inventory.lookupPart(searchPart);
            IC_part tempPart = IC_inventory.allParts().get(index);

            ObservableList<IC_part> tempProdList = FXCollections.observableArrayList();
            tempProdList.add(tempPart);
            AddTableView.setItems(tempProdList);
        }
    }


    @FXML
    void AddPartBtn(ActionEvent event) {
        IC_part part = AddTableView.getSelectionModel().getSelectedItem();
        currentParts.add(part);
        updateCurrentPartTable();
    }

    @FXML
    void SearchPartDeleteBtn(ActionEvent event) {

        String searchPart = DelPartSearchField.getText();
        int index;

        if (IC_inventory.lookupPart(searchPart) == -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Part not found");
            alert.setContentText("Part not found");
            alert.showAndWait();
        } else {
            index = IC_inventory.lookupPart(searchPart);
            IC_part tempPart = IC_inventory.allParts().get(index);

            ObservableList<IC_part> list = FXCollections.observableArrayList();
            list.add(tempPart);
            DeleteTableView.setItems(list);
        }
    }



    @FXML
    void DeletePartBtn(ActionEvent event) {

        IC_part part = DeleteTableView.getSelectionModel().getSelectedItem();
        Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION);
        deleteAlert.initModality(Modality.NONE);
        deleteAlert.setTitle("Delete");
        deleteAlert.setHeaderText("Do you want to delete " + part.getName() + " ?");
        deleteAlert.setContentText("Delete Part");

        deleteAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                currentParts.remove(part);
            }
        });
    }


    @FXML
    void SaveBtnClicked(ActionEvent event) throws IOException {

        String productName = NameField.getText();
        String productInv = InvCountField.getText();
        String productPrice = PriceField.getText();
        String productMin = MinField.getText();
        String productMax = MaxField.getText();

        try {

            errmsg = IC_product.isProductValid(productName, Integer.parseInt(productMin), Integer.parseInt(productMax), Integer.parseInt(productInv),
                    Double.parseDouble(productPrice), currentParts, errmsg);

            if (errmsg.length() > 0) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText(errmsg);
                alert.showAndWait();
                errmsg = "";
            } else {

                IC_product newProduct = new IC_product();

                newProduct.setProductID(productID);
                newProduct.setName(productName);
                newProduct.setPrice(Double.parseDouble(productPrice));
                newProduct.setInStock(Integer.parseInt(productInv));
                newProduct.setMin(Integer.parseInt(productMin));
                newProduct.setMax(Integer.parseInt(productMax));
                newProduct.setParts(currentParts);
                IC_inventory.addProduct(newProduct);

                Parent productsSave = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                Scene scene = new Scene(productsSave);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

                window.setScene(scene);
                window.show();
            }
        } catch (NumberFormatException e) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText("No Blank Fields Allowed");
            alert.showAndWait();
        }
    }

    @FXML
    void CancelBtnClicked(ActionEvent event) {

        Alert cancelAlert = new Alert(Alert.AlertType.CONFIRMATION);
        cancelAlert.initModality(Modality.NONE);
        cancelAlert.setTitle("Cancel");
        cancelAlert.setHeaderText("Cancel");
        cancelAlert.setContentText("Cancel and go to main screen?");

        cancelAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                Parent Cancel = null;
                try {
                    Cancel = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Scene scene = new Scene(Cancel);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        });

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        PartIDCol.setCellValueFactory(cellData -> cellData.getValue().partID().asObject());
        PartNameCol.setCellValueFactory(cellData -> cellData.getValue().Name());
        InvLevelCol.setCellValueFactory(cellData -> cellData.getValue().inStock().asObject());
        PriceCol.setCellValueFactory(cellData -> cellData.getValue().Price().asObject());
        CurrentPartIDCol.setCellValueFactory(cellData -> cellData.getValue().partID().asObject());
        CurrentPartNameCol.setCellValueFactory(cellData -> cellData.getValue().Name());
        CurrentInvCol.setCellValueFactory(cellData -> cellData.getValue().inStock().asObject());
        CurrentPriceCol.setCellValueFactory(cellData -> cellData.getValue().Price().asObject());

        updatePartTable();
        updateCurrentPartTable();

        productID = IC_inventory.getProductCount();
        IDField.setText(String.valueOf(productID));
    }

    private void updatePartTable() {
        AddTableView.setItems(IC_inventory.allParts());
    }

    private void updateCurrentPartTable() {
        DeleteTableView.setItems(currentParts);
    }

}
