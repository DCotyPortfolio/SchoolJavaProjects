//Student Class for Scripting and Programming Applications Performance Assessment

// by David Coty, Western Governors University, August 2017


import java.util.Arrays;

public class Student {

    private int StudentID;
    private String FirstName;
    private String LastName;
    private String EmailAddress;
    private int Age;
    private int[] Grades;

    //Student Constructor
    public Student(int StudentID, String FirstName, String LastName, String EmailAddress, int Age, int[] Grades) {

        setStudentID(StudentID);
        setFirstName(FirstName);
        setLastName(LastName);
        setEmailAddress(EmailAddress);
        setAge(Age);
        setGrades(Grades);

    }


    //Getter and Setter - StudentID
    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID( int StudentID) {
        this.StudentID = StudentID;

    }


    //Getter and Setter - FirstName
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName( String FirstName) {
        this.FirstName = FirstName;

    }


    //Getter and Setter - LastName
    public String getLastName() {
        return LastName;
    }

    public void setLastName( String LastName) {
        this.LastName = LastName;

    }


    //Getter and Setter - EmailAddress
    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress( String EmailAddress) {
        this.EmailAddress = EmailAddress;

    }

    //Getter and Setter - Age
    public int getAge() {
        return Age;
    }

    public void setAge( int Age) {
        this.Age = Age;

    }

    //Getter and Setter - Grades
    public int[] getGrades(){
        return Grades;
    }

    public void setGrades(int[] Grades){
       this.Grades = Grades;
    }


    //Print method
    //Use this in the Roster Class print all function.
    public void print(){
        System.out.println(
                "Student ID:\t" + getStudentID() +
                        "\tFirst Name:\t" + getFirstName() +
                        "\tLast Name:\t" + getLastName() +
                        "\tEmail Address:\t" + getEmailAddress() +
                        "\tAge:\t" + getAge() +
                        "\tGrades:\t" + Arrays.toString(getGrades()));
    }

}