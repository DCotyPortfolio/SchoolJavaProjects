package controllers;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.IC_inventory;
import models.IC_part;
import models.IC_product;


//created on 3/18/18 by David Coty
public class IC_mainController implements Initializable {

    @FXML
    private TableView<IC_part> PartsTable;
    @FXML
    private TableColumn<IC_part, Integer> PartIDCol;
    @FXML
    private TableColumn<IC_part, String> PartNameCol;
    @FXML
    private TableColumn<IC_part, Integer> PartInvCol;
    @FXML
    private TableColumn<IC_part, Double> PartPriceCol;
    @FXML
    private TableView<IC_product> ProductsTable;
    @FXML
    private TableColumn<IC_product, Integer> ProductIDCol;
    @FXML
    private TableColumn<IC_product, String> ProductNameCol;
    @FXML
    private TableColumn<IC_product, Integer> ProductInvCol;
    @FXML
    private TableColumn<IC_product, Double> ProductPriceCol;
    @FXML
    private TextField PartsSearchTxtField;
    @FXML
    private TextField ProductsSearchTxtField;

    private static int modPartIndex;
    private static int modProductIndex;

    public static int partToModifyIndex() {
        return modPartIndex;
    }

    public static int productToModifyIndex() {
        return modProductIndex;
    }

    public IC_mainController() {
    }

    @FXML
    void ClearPartSearch(ActionEvent event) {
        UpdatePartTable();
        PartsSearchTxtField.setText("");
    }

    @FXML
    void ClearProductSearch(ActionEvent event) {
        UpdateProductTable();
        ProductsSearchTxtField.setText("");
    }

    @FXML
    void ExitClick(ActionEvent event) {

        Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
        exitAlert.initModality(Modality.NONE);
        exitAlert.setTitle("Exit Application");
        exitAlert.setHeaderText("Exit Application");
        exitAlert.setContentText("Exit Application?");

        exitAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                System.exit(0);
            }
        });
    }

        @FXML
    void AddParts(ActionEvent event) throws IOException {

        Parent addParts = FXMLLoader.load(getClass().getResource("/views/IC_addPart.fxml"));
        Scene scene = new Scene(addParts);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    @FXML
    void AddProducts(ActionEvent event) throws IOException {

        Parent addProducts = FXMLLoader.load(getClass().getResource("/views/IC_addProduct.fxml"));
        Scene scene = new Scene(addProducts);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    @FXML
    void ModifyParts(ActionEvent event) throws IndexOutOfBoundsException, IOException {

        IC_part modifyPart = PartsTable.getSelectionModel().getSelectedItem();
        if (modifyPart == null) {


        } else {
            modPartIndex = IC_inventory.allParts().indexOf(modifyPart);
            Parent modifyParts = FXMLLoader.load(getClass().getResource("/views/IC_modifyPart.fxml"));
            Scene scene = new Scene(modifyParts);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();
        }
    }

    @FXML
    void ModifyProducts(ActionEvent event) throws IOException {

        IC_product modifyProduct = ProductsTable.getSelectionModel().getSelectedItem();

        if (modifyProduct == null) {
           //nothing selected to modify, do nothing.

        } else {

            modProductIndex = IC_inventory.products().indexOf(modifyProduct);
            Parent modifyProducts = FXMLLoader.load(getClass().getResource("/views/IC_modifyProduct.fxml"));
            Scene scene = new Scene(modifyProducts);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();
        }
    }
    @FXML
    void SearchProducts(ActionEvent event) {

        String searchProd = ProductsSearchTxtField.getText();
        int prodIndex = -1;
        if (ProductsSearchTxtField.getText().isEmpty()){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Search Error");
            alert.setHeaderText("Search Field is empty");
            alert.setContentText("Search Field is empty. ");
            alert.showAndWait();
        }

        else if (IC_inventory.lookupProduct(searchProd) != -1) {
            prodIndex = IC_inventory.lookupProduct(searchProd);
            IC_product tempProd = IC_inventory.products().get(prodIndex);
            ObservableList<IC_product> tempProdList = FXCollections.observableArrayList();
            tempProdList.add(tempProd);
            ProductsTable.setItems(tempProdList);
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Product not found");
            alert.setContentText("Product is not found");
            alert.showAndWait();
        }
    }

    @FXML
    void DeleteProducts(ActionEvent event) {

        IC_product product = ProductsTable.getSelectionModel().getSelectedItem();

        if(product != null){
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Confirm Delete");
            alert.setHeaderText("Confirm");
            alert.setContentText("Are you sure you want to delete " + product.getName() + "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                IC_inventory.removeProduct(product);
                UpdateProductTable();

            } else {

            }
        }

    }

    @FXML
    void SearchParts(ActionEvent event) {

        String searchPart = PartsSearchTxtField.getText();

       int partIndex = -1;
        // check to see if the search field is empty

        if (searchPart.isEmpty()){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Search Error");
            alert.setHeaderText("Search Field is empty");
            alert.setContentText("Search Field is empty.");
            alert.showAndWait();

        } else if ((IC_inventory.lookupPart(searchPart) != -1)) {

           partIndex = IC_inventory.lookupPart(searchPart);
           IC_part tempPart = IC_inventory.allParts().get(partIndex);
           ObservableList<IC_part> ProductList = FXCollections.observableArrayList();
           ProductList.add(tempPart);
           PartsTable.setItems(ProductList);

       } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Search Error");
            alert.setHeaderText("part not found");
            alert.setContentText("Part is not found");
            alert.showAndWait();
        }
    }

    @FXML
    void DeleteParts(ActionEvent event) {

        IC_part part = PartsTable.getSelectionModel().getSelectedItem();
        if (IC_inventory.validatePartDelete(part)) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Part Delete Error!");
            alert.setHeaderText("Part cannot be removed!");
            alert.setContentText("This part is used in a product.");
            alert.showAndWait();
        } else if (part != null) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Product Delete");
            alert.setHeaderText("Confirm?");
            alert.setContentText("Are you sure you want to delete " + part.getName() + "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                IC_inventory.deletePart(part);
                UpdatePartTable();
            } else {
                if (result.get() == ButtonType.CANCEL) {
                    //nothing to handle here, the box will close
                }

            }

        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        PartIDCol.setCellValueFactory(cellData -> cellData.getValue().partID().asObject());
        PartNameCol.setCellValueFactory(cellData -> cellData.getValue().Name());
        PartInvCol.setCellValueFactory(cellData -> cellData.getValue().inStock().asObject());
        PartPriceCol.setCellValueFactory(cellData -> cellData.getValue().Price().asObject());
        ProductIDCol.setCellValueFactory(cellData -> cellData.getValue().ID().asObject());
        ProductNameCol.setCellValueFactory(cellData -> cellData.getValue().name());
        ProductInvCol.setCellValueFactory(cellData -> cellData.getValue().Inv().asObject());
        ProductPriceCol.setCellValueFactory(cellData -> cellData.getValue().price().asObject());
        UpdatePartTable();
        UpdateProductTable();
    }

    private void UpdatePartTable() {
        PartsTable.setItems(IC_inventory.allParts());
    }

    private void UpdateProductTable() {

        ProductsTable.setItems(IC_inventory.products());
    }


}
