package controllers;


import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.IC_inventory;
import models.IC_part;
import models.IC_product;



//created on 3/18/18 by David Coty
public class IC_modifyProductController implements Initializable {

    private ObservableList currentParts = FXCollections.observableArrayList();
    private final int prodIndex = IC_mainController.productToModifyIndex();
    private String errmsg = "";
    private int productID;

    @FXML
    private TextField IDField;
    @FXML
    private TextField MinField;
    @FXML
    private TextField MaxField;
    @FXML
    private TextField InvCountField;
    @FXML
    private TextField NameField;
    @FXML
    private TextField PriceField;
    @FXML
    private TextField AddPartSearchField;
    @FXML
    private TextField DeletePartSearchField;
    @FXML
    private TableView<IC_part> AddTableView;
    @FXML
    private TableColumn<IC_part, Integer> PartIDCol;
    @FXML
    private TableColumn<IC_part, String> PartNameCol;
    @FXML
    private TableColumn<IC_part, Integer> PartInvCol;
    @FXML
    private TableColumn<IC_part, Double> PartPriceCol;
    @FXML
    private TableView<IC_part> DeleteTableView;
    @FXML
    private TableColumn<IC_part, Integer> CurrentPartIDCol;
    @FXML
    private TableColumn<IC_part, String> CurrentPartNameCol;
    @FXML
    private TableColumn<IC_part, Integer> CurrentPartInvCol;
    @FXML
    private TableColumn<IC_part, Double> CurrentPartPriceCol;

    @FXML
    void ClearSearchAdd(ActionEvent event) {
        updatePartView();
        AddPartSearchField.setText("");
    }

    @FXML
    void ClearSearchRemove(ActionEvent event) {
        updateCurrPartView();
        DeletePartSearchField.setText("");
    }

    @FXML
    void SearchPartAddBtn(ActionEvent event) {
        String searchPart = AddPartSearchField.getText();
        int partIndex;
        if (IC_inventory.lookupPart(searchPart) == -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search");
            alert.setHeaderText("Part not found");
            alert.setContentText("Search term not found.");
            alert.showAndWait();
        } else {
            partIndex = IC_inventory.lookupPart(searchPart);
            IC_part tempPart = IC_inventory.allParts().get(partIndex);
            ObservableList<IC_part> tempProdList = FXCollections.observableArrayList();
            tempProdList.add(tempPart);
            AddTableView.setItems(tempProdList);
        }
    }

    @FXML
    void AddBtn(ActionEvent event) {
        IC_part part = AddTableView.getSelectionModel().getSelectedItem();
        currentParts.add(part);
        updateCurrPartView();
    }

    @FXML
    void SearchPartDeleteBtn(ActionEvent event) {
        String searchPart = DeletePartSearchField.getText();
        int partIndex;

        if (IC_inventory.lookupPart(searchPart) == -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search");
            alert.setHeaderText("Part not found");
            alert.setContentText("Search term not found.");
            alert.showAndWait();
        } else {
            partIndex = IC_inventory.lookupPart(searchPart);
            IC_part tempPart = IC_inventory.allParts().get(partIndex);

            ObservableList<IC_part> tempProdList = FXCollections.observableArrayList();
            tempProdList.add(tempPart);
            DeleteTableView.setItems(tempProdList);
        }
    }

    @FXML
    void DeleteBtn(ActionEvent event) {
        IC_part part = DeleteTableView.getSelectionModel().getSelectedItem();

        if (currentParts.size() == 0) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText("Product has no parts to delete. ");
            alert.showAndWait();

        }  else

        {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirm Part Delete");
            alert.setContentText("Are you sure you want to delete " + part.getName() + " ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                currentParts.remove(part);
            } else {

            }
        }
    }
    @FXML
    void SaveBtn(ActionEvent event) throws IOException {
        String productName = NameField.getText();
        String productInv = InvCountField.getText();
        String productPrice = PriceField.getText();
        String productMin = MinField.getText();
        String productMax = MaxField.getText();
        try {
            errmsg = IC_product.isProductValid(productName, Integer.parseInt(productMin), Integer.parseInt(productMax), Integer.parseInt(productInv),
                    Double.parseDouble(productPrice), currentParts, errmsg);

            if (errmsg.length() > 0 ) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Adding Product!");
                alert.setHeaderText("Error!");
                alert.setContentText(errmsg);
                alert.showAndWait();
            }
             else
                {

                IC_product newProduct = new IC_product();
                newProduct.setProductID(productID);
                newProduct.setName(productName);
                newProduct.setPrice(Double.parseDouble(productPrice));
                newProduct.setInStock(Integer.parseInt(productInv));
                newProduct.setMin(Integer.parseInt(productMin));
                newProduct.setMax(Integer.parseInt(productMax));
                newProduct.setParts(currentParts);
                IC_inventory.updateProduct(prodIndex, newProduct);

                Parent productsSave = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                Scene scene = new Scene(productsSave);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Adding Product");
            alert.setHeaderText("Error");
            alert.setContentText("Fields cannot be blank");
            alert.showAndWait();
        }
    }

    @FXML
    void CancelBtn(ActionEvent event) {

        Alert cancelAlert = new Alert(Alert.AlertType.CONFIRMATION);
        cancelAlert.initModality(Modality.NONE);
        cancelAlert.setTitle("Cancel");
        cancelAlert.setHeaderText("Cancel");
        cancelAlert.setContentText("Cancel and go to main screen?");

        cancelAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                Parent productsCancel = null;
                try {
                    productsCancel = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Scene scene = new Scene(productsCancel);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        });


    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        IC_product product = IC_inventory.products().get(prodIndex);
        productID = IC_inventory.products().get(prodIndex).getProductID();
        IDField.setText(String.valueOf(productID));
        NameField.setText(product.getName());
        InvCountField.setText(Integer.toString(product.getInStock()));
        PriceField.setText(Double.toString(product.getPrice()));
        MinField.setText(Integer.toString(product.getMin()));
        MaxField.setText(Integer.toString(product.getMax()));
        currentParts = product.getParts();
        PartIDCol.setCellValueFactory(cellData -> cellData.getValue().partID().asObject());
        PartNameCol.setCellValueFactory(cellData -> cellData.getValue().Name());
        PartInvCol.setCellValueFactory(cellData -> cellData.getValue().inStock().asObject());
        PartPriceCol.setCellValueFactory(cellData -> cellData.getValue().Price().asObject());
        CurrentPartIDCol.setCellValueFactory(cellData -> cellData.getValue().partID().asObject());
        CurrentPartNameCol.setCellValueFactory(cellData -> cellData.getValue().Name());
        CurrentPartInvCol.setCellValueFactory(cellData -> cellData.getValue().inStock().asObject());
        CurrentPartPriceCol.setCellValueFactory(cellData -> cellData.getValue().Price().asObject());
        updatePartView();
        updateCurrPartView();
    }

    private void updatePartView() {
        AddTableView.setItems(IC_inventory.allParts());
    }

    private void updateCurrPartView() {
        DeleteTableView.setItems(currentParts);
    }




}
