package Controller;

import Model.UserModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;

//created on 6/4/18 by David Coty
public class MainMenuController {
    @FXML
    private MenuItem logoutUser;
    private MainController mainApp;
    private UserModel currentUser;

    //constructor
    public MainMenuController() {

    }


    public void setMenu(MainController mainApp, UserModel currentUser) {
        this.mainApp = mainApp;
        this.currentUser = currentUser;

        logoutUser.setText("Logout: " + currentUser.getUsername());
    }


    @FXML
    void menuAppointments(ActionEvent event) {
        mainApp.showAppointmentScreen(currentUser);
    }


    @FXML
    void menuCustomers(ActionEvent event) {
        mainApp.showCustomerScreen(currentUser);
    }


    @FXML
    void menuReports(ActionEvent event) {
        mainApp.showReports(currentUser);

    }


    @FXML
    void menuLogout(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Logout");
        alert.setHeaderText("Do you want to logout?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> mainApp.showLogin());
    }


    @FXML
    void menuClose(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Close");
        alert.setHeaderText("Close the program?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent((ButtonType response) -> {
                            Platform.exit();
                            System.exit(0);
                        }
                );
    }
}
