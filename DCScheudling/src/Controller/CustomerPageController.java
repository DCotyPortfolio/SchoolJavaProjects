package Controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import Model.CityModel;
import Model.CustomerModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.StringConverter;



public class CustomerPageController {
    @FXML
    private TableView<CustomerModel> customerTable;

    @FXML
    private TableColumn<CustomerModel, String> customerNameColumn;

    @FXML
    private TableColumn<CustomerModel, String> phoneColumn;

    @FXML
    private TextField customerIdField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField addressField;

    @FXML
    private ComboBox<CityModel> cityComboBox;

    @FXML
    private TextField address2Field;

    @FXML
    private TextField postalCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField countryField;

    @FXML
    private ButtonBar newEditDeleteButtonBar;

    @FXML
    private ButtonBar saveCancelButtonBar;

    private MainController mainApp;
    private boolean editClicked = false;
    private Stage dialogStage;
    private UserModel currentUser;

    public CustomerPageController() {
    }

    @FXML
    void handleNewCustomer(ActionEvent event) {
        editClicked = false;
        enableCustomerFields();
        saveCancelButtonBar.setDisable(false);
        customerTable.setDisable(true);
        clearCustomerDetails();
        customerIdField.setText("Auto-Generated");
        newEditDeleteButtonBar.setDisable(true);
    }

    @FXML
    void handleEditCustomer(ActionEvent event) {
        CustomerModel selectedCustomer = customerTable.getSelectionModel().getSelectedItem();

        if (selectedCustomer != null) {
            editClicked = true;
            enableCustomerFields();
            saveCancelButtonBar.setDisable(false);
            customerTable.setDisable(true);
            newEditDeleteButtonBar.setDisable(true);
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Customer selected");
            alert.setContentText("Please select a Customer in the Table");
            alert.showAndWait();
        }


    }

    @FXML
    void handleDeleteCustomer(ActionEvent event) {
        CustomerModel selectedCustomer = customerTable.getSelectionModel().getSelectedItem();

        if (selectedCustomer != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm Deletion");
            alert.setHeaderText("Are you sure you want to delete " + selectedCustomer.getCustomerName() + "?");
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> {
                                deleteCustomer(selectedCustomer);
                                mainApp.showCustomerScreen(currentUser);
                            }
                    );
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Customer selected for Deletion");
            alert.setContentText("Please select a Customer in the Table to delete");
            alert.showAndWait();
        }

    }

    @FXML
    void handleSaveCustomer(ActionEvent event) {
        if (validateCustomer()) {
            saveCancelButtonBar.setDisable(true);
            customerTable.setDisable(false);
            if (editClicked == true) {
                //edits Customer record
                updateCustomer();
            } else if (editClicked == false) {
                //inserts new customer record
                saveCustomer();
            }
            mainApp.showCustomerScreen(currentUser);
        }
    }

    @FXML
    void handleCancelCustomer(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Cancel");
        alert.setHeaderText("Are you sure you want to Cancel?");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                            saveCancelButtonBar.setDisable(true);
                            customerTable.setDisable(false);
                            clearCustomerDetails();
                            newEditDeleteButtonBar.setDisable(false);
                            editClicked = false;
                        }
                );
    }


    public void setCustomerScreen(MainController mainApp, UserModel currentUser) {
        this.mainApp = mainApp;
        this.currentUser = currentUser;

        customerNameColumn.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phone"));
        disableCustomerFields();

        populateCityList();

        cityComboBox.setConverter(new StringConverter<CityModel>() {

            @Override
            public String toString(CityModel object) {
                return object.getCity();
            }

            @Override
            public CityModel fromString(String string) {
                return cityComboBox.getItems().stream().filter(ap ->
                        ap.getCity().equals(string)).findFirst().orElse(null);
            }
        });

        cityComboBox.valueProperty().addListener((obs, oldval, newval) -> {
            if (newval != null)
                showCountry(newval.toString());
        });

        customerTable.getItems().setAll(populateCustomerList());
        customerTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showCustomerDetails(newValue));

    }


    @FXML
    private void showCustomerDetails(CustomerModel selectedCustomer) {

        customerIdField.setText(selectedCustomer.getCustomerId());
        nameField.setText(selectedCustomer.getCustomerName());
        addressField.setText(selectedCustomer.getAddress());
        address2Field.setText(selectedCustomer.getAddress2());
        cityComboBox.setValue(selectedCustomer.getCity());
        countryField.setText(selectedCustomer.getCountry());
        postalCodeField.setText(selectedCustomer.getPostalCode());
        phoneField.setText(selectedCustomer.getPhone());

    }

    //disables editing to not allow entry prior to clicking New or Edit
    private void disableCustomerFields() {

        nameField.setEditable(false);
        addressField.setEditable(false);
        address2Field.setEditable(false);
        postalCodeField.setEditable(false);
        phoneField.setEditable(false);
    }

    //enables editing after New or Edit clicked
    private void enableCustomerFields() {

        nameField.setEditable(true);
        addressField.setEditable(true);
        address2Field.setEditable(true);
        postalCodeField.setEditable(true);
        phoneField.setEditable(true);
    }

    //clears details listed in fields
    @FXML
    private void clearCustomerDetails() {

        customerIdField.clear();
        nameField.clear();
        addressField.clear();
        address2Field.clear();
        countryField.clear();
        postalCodeField.clear();
        phoneField.clear();

    }


    protected List<CustomerModel> populateCustomerList() {

        String tCustomerId;
        String tCustomerName;
        String tAddress;
        String tAddress2;
        CityModel tCity;
        String tCountry;
        String tPostalCode;
        String tPhone;

        ObservableList<CustomerModel> customerList = FXCollections.observableArrayList();
        try (


                PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement(
                        "SELECT customer.customerId, customer.customerName, address.address, address.address2, address.postalCode, city.cityId, city.city, country.country, address.phone " +
                                "FROM customer, address, city, country " +
                                "WHERE customer.addressId = address.addressId AND address.cityId = city.cityId AND city.countryId = country.countryId " +
                                "ORDER BY customer.customerName");
                ResultSet rs = statement.executeQuery()) {


            while (rs.next()) {
                tCustomerId = rs.getString("customer.customerId");

                tCustomerName = rs.getString("customer.customerName");

                tAddress = rs.getString("address.address");

                tAddress2 = rs.getString("address.address2");

                tCity = new CityModel(rs.getInt("city.cityId"), rs.getString("city.city"));

                tCountry = rs.getString("country.country");

                tPostalCode = rs.getString("address.postalCode");

                tPhone = rs.getString("address.phone");

                customerList.add(new CustomerModel(tCustomerId, tCustomerName, tAddress, tAddress2, tCity, tCountry, tPostalCode, tPhone));

            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
           e.printStackTrace();
        }


        return customerList;

    }


    protected void populateCityList() {


        ObservableList<CityModel> cities = FXCollections.observableArrayList();

        try (

                PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement("SELECT cityId, city FROM city LIMIT 100;");
                ResultSet rs = statement.executeQuery();) {

            while (rs.next()) {
                cities.add(new CityModel(rs.getInt("city.cityId"), rs.getString("city.city")));
            }


        } catch (SQLException sqe) {

            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cityComboBox.setItems(cities);

    }


    @FXML
    private void showCountry(String citySelection) {
        String tcity;
        String tcountry;

        try {
            PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement("SELECT  city.city, country.country FROM city INNER JOIN country ON country.countryid=city.countryId;");
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                tcity = rs.getString("city.city");
                tcountry = rs.getString("country.country");

                if(citySelection.equals(tcity)){
                    countryField.setText(tcountry);
                }

            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveCustomer() {

        try {

            PreparedStatement ps = DBConnectionUtility.getDBConnection().prepareStatement("INSERT INTO address (address, address2, cityId, postalCode, phone, createDate, createdBy, lastUpdate, lastUpdateBy) "
                    + "VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, ?)", Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, addressField.getText());
            ps.setString(2, address2Field.getText());
            ps.setInt(3, cityComboBox.getValue().getCityID());
            ps.setString(4, postalCodeField.getText());
            ps.setString(5, phoneField.getText());
            ps.setString(6, currentUser.getUsername());
            ps.setString(7, currentUser.getUsername());
            boolean res = ps.execute();
            int newAddressId = -1;
            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                newAddressId = rs.getInt(1);

            }


            PreparedStatement psc = DBConnectionUtility.getDBConnection().prepareStatement("INSERT INTO customer "
                    + "(customerName, addressId, active, createDate, createdBy, lastUpdate, lastUpdateBy)"
                    + "VALUES (?, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP, ?)");

            psc.setString(1, nameField.getText());
            psc.setInt(2, newAddressId);
            psc.setInt(3, 1);
            //psc.setString(4, LocalDateTime.now().toString());
            psc.setString(4, currentUser.getUsername());
            //psc.setString(6, LocalDateTime.now().toString());
            psc.setString(5, currentUser.getUsername());
            int result = psc.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    private void deleteCustomer(CustomerModel customer) {

        try {
            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement("DELETE customer.*, address.* from customer, address WHERE customer.customerId = ? AND customer.addressId = address.addressId");
            pst.setString(1, customer.getCustomerId());
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void updateCustomer() {
        try {

            PreparedStatement ps = DBConnectionUtility.getDBConnection().prepareStatement("UPDATE address, customer, city, country "
                    + "SET address = ?, address2 = ?, address.cityId = ?, postalCode = ?, phone = ?, address.lastUpdate = CURRENT_TIMESTAMP, address.lastUpdateBy = ? "
                    + "WHERE customer.customerId = ? AND customer.addressId = address.addressId AND address.cityId = city.cityId AND city.countryId = country.countryId");

            ps.setString(1, addressField.getText());
            ps.setString(2, address2Field.getText());
            ps.setInt(3, cityComboBox.getValue().getCityID());
            ps.setString(4, postalCodeField.getText());
            ps.setString(5, phoneField.getText());
            ps.setString(6, currentUser.getUsername());
            ps.setString(7, customerIdField.getText());

            int result = ps.executeUpdate();


            PreparedStatement psc = DBConnectionUtility.getDBConnection().prepareStatement("UPDATE customer, address, city "
                    + "SET customerName = ?, customer.lastUpdate = CURRENT_TIMESTAMP, customer.lastUpdateBy = ? "
                    + "WHERE customer.customerId = ? AND customer.addressId = address.addressId AND address.cityId = city.cityId");

            psc.setString(1, nameField.getText());
            psc.setString(2, currentUser.getUsername());
            psc.setString(3, customerIdField.getText());
            int results = psc.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    private boolean validateCustomer() {
        String name = nameField.getText();
        String address = addressField.getText();
        CityModel city = cityComboBox.getValue();
        String country = countryField.getText();
        String zip = postalCodeField.getText();
        String phone = phoneField.getText();

        String errorMessage = "";
        if (name == null || name.length() == 0) {
            errorMessage += "Enter name for customer.\n";
        }
        if (address == null || address.length() == 0) {
            errorMessage += "Enter address for customer.\n";
        }
        if (city == null) {
            errorMessage += "Select a City.\n";
        }
        if (country == null || country.length() == 0) {
            errorMessage += "Country Invalid. Country is set by city selection.\n";
        }
        if (zip == null || zip.length() == 0) {
            errorMessage += "Enter a postal code.\n";
        } else if (zip.length() > 10 || zip.length() < 5) {
            errorMessage += "Enter valid postal code.\n";
        }
        if (phone == null || phone.length() == 0) {
            errorMessage += "Enter area code and phone number.";
        } else if (phone.length() < 10 || phone.length() > 15) {
            errorMessage += "Enter a valid area code and phone number.\n";
        }
        if (errorMessage.length() == 0) {
            return true;
        } else {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Some or all Fields are Invalid.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }


    }
}





