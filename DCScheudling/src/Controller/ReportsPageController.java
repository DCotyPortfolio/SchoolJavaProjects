package Controller;

import Model.AppointmentModel;
import Model.AppointmentReportModel;
import Model.CustomerModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.chart.XYChart.Data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

//created on 6/4/18 by David Coty
public class ReportsPageController {

    //report Fields
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab schedTab;
    @FXML
    private TableView<AppointmentModel> schedTableView;
    @FXML
    private TableColumn<AppointmentModel, ZonedDateTime> startSchedColumn;
    @FXML
    private TableColumn<AppointmentModel, LocalDateTime> endSchedColumn;
    @FXML
    private TableColumn<AppointmentModel, String> titleSchedColumn;
    @FXML
    private TableColumn<AppointmentModel, String> typeSchedColumn;
    @FXML
    private TableColumn<AppointmentModel, CustomerModel> customerSchedColumn;
    @FXML
    private Tab apptTab;
    @FXML
    private TableView<AppointmentReportModel> apptTableView;
    @FXML
    private TableColumn<AppointmentModel, String> monthColumn;
    @FXML
    private TableColumn<AppointmentModel, String> typeColumn;
    @FXML
    private TableColumn<AppointmentModel, String> typeAmount;
    @FXML
    private Tab custTab;
    @FXML
    private BarChart barChart;
    @FXML
    private CategoryAxis xAxis;
    @FXML
    private NumberAxis yAxis;


    private MainController mainApp;

    private ObservableList<AppointmentReportModel> apptList;
    private ObservableList<AppointmentModel> schedule;
    private ObservableList<PieChart.Data> pieChartData;
    private final DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    private final ZoneId newzid = ZoneId.systemDefault();
    private UserModel currentUser;

    public ReportsPageController() {

    }


    public void setReports(MainController mainApp, UserModel currentUser) {
        this.mainApp = mainApp;
        this.currentUser = currentUser;

        //methods called to populate data on each tab
        populateApptTypeList();
        populateCustBarChart();
        populateSchedule();

        startSchedColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
        endSchedColumn.setCellValueFactory(new PropertyValueFactory<>("end"));
        titleSchedColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        typeSchedColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        customerSchedColumn.setCellValueFactory(new PropertyValueFactory<>("customer"));

        monthColumn.setCellValueFactory(new PropertyValueFactory<>("Month"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("Type"));
        typeAmount.setCellValueFactory(new PropertyValueFactory<>("Amount"));


    }


    private void populateApptTypeList() {
        apptList = FXCollections.observableArrayList();

        try {


            PreparedStatement statement = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT MONTHNAME(`start`) AS \"Month\", description AS \"Type\", COUNT(*) as \"Amount\" "
                            + "FROM appointment "
                            + "GROUP BY MONTHNAME(`start`), description");
            ResultSet rs = statement.executeQuery();


            while (rs.next()) {

                String month = rs.getString("Month");

                String type = rs.getString("Type");

                String amount = rs.getString("Amount");

                apptList.add(new AppointmentReportModel(month, type, amount));


            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        apptTableView.getItems().addAll(apptList);

    }


    private void populateCustBarChart() {

        ObservableList<XYChart.Data<String, Integer>> data = FXCollections.observableArrayList();
        XYChart.Series<String, Integer> series = new XYChart.Series<>();

        try {
            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT city.city, COUNT(city) "
                            + "FROM customer, address, city "
                            + "WHERE customer.addressId = address.addressId "
                            + "AND address.cityId = city.cityId "
                            + "GROUP BY city");
            ResultSet rs = pst.executeQuery();


            while (rs.next()) {
                String city = rs.getString("city");
                Integer count = rs.getInt("COUNT(city)");
                data.add(new Data<>(city, count));
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        series.getData().addAll(data);
        barChart.getData().add(series);
    }

    private void populateSchedule() {

        schedule = FXCollections.observableArrayList();

        try {

            PreparedStatement pst = DBConnectionUtility.getDBConnection().prepareStatement(
                    "SELECT appointment.appointmentId, appointment.customerId, appointment.title, appointment.description, "
                            + "appointment.`start`, appointment.`end`, customer.customerId, customer.customerName, appointment.createdBy "
                            + "FROM appointment, customer "
                            + "WHERE appointment.customerId = customer.customerId AND appointment.`start` >= CURRENT_DATE AND appointment.createdBy = ?"
                            + "ORDER BY `start`");
            pst.setString(1, currentUser.getUsername());
            ResultSet rs = pst.executeQuery();


            while (rs.next()) {

                String tAppointmentId = rs.getString("appointment.appointmentId");
                Timestamp tsStart = rs.getTimestamp("appointment.start");
                ZonedDateTime newzdtStart = tsStart.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalStart = newzdtStart.withZoneSameInstant(newzid);

                Timestamp tsEnd = rs.getTimestamp("appointment.end");
                ZonedDateTime newzdtEnd = tsEnd.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalEnd = newzdtEnd.withZoneSameInstant(newzid);

                String tTitle = rs.getString("appointment.title");

                String tType = rs.getString("appointment.description");

                CustomerModel tCustomer = new CustomerModel(rs.getString("appointment.customerId"), rs.getString("customer.customerName"));

                String tUser = rs.getString("appointment.createdBy");

                schedule.add(new AppointmentModel(tAppointmentId, newLocalStart.format(timeDTF), newLocalEnd.format(timeDTF), tTitle, tType, tCustomer, tUser));


            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
           e.printStackTrace();
        }
        schedTableView.getItems().setAll(schedule);
    }

}




