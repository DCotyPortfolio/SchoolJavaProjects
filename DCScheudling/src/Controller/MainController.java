package Controller;

import Model.AppointmentModel;
import Model.UserModel;
import Utility.DBConnectionUtility;
import Utility.LoggingUtility;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.util.Locale;



public class MainController extends Application {

    private Stage primaryStage;
    private BorderPane menu;
    private AnchorPane loginScreen;
    private AnchorPane customerScreen;
    private AnchorPane appointmentScreen;
    private AnchorPane custReportScreen;
    private TabPane tabPane;
    Locale locale = Locale.getDefault();
    private static Connection connection;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Scheduling Application");
        showLogin();
    }

    public static void main(String[] args) {
        DBConnectionUtility.initialize();
        connection = DBConnectionUtility.getDBConnection();
        LoggingUtility.initalizeLog();
        launch(args);
        DBConnectionUtility.closeDBConnection();
    }

    public void showLogin() {

        try {
            // Load Login Screen.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/LoginPage.fxml"));
            AnchorPane loginScreen = (AnchorPane) loader.load();

            // Give the controller access to the main app.
            LoginPageController LoginController = loader.getController();
            LoginController.setLogin(this);

            // Show the scene containing the root layout.
            Scene scene = new Scene(loginScreen);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();


        }
    }

    public void showAppMenu(UserModel currentUser) {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/MainMenu.fxml"));
            menu = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(menu);
            primaryStage.setScene(scene);
            // Give the controller access to the main app.
            MainMenuController controller = loader.getController();
            controller.setMenu(this, currentUser);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showAppointmentScreen(UserModel currentUser) {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/AppointmentPage.fxml"));
            AnchorPane appointmentScreen = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            menu.setCenter(appointmentScreen);

           //lock the screen size for appointments
            primaryStage.setMinWidth(800);
            primaryStage.setMinHeight(625);

            // Give the controller access to the main app.
            AppointmentPageController controller = loader.getController();
            controller.setAppointmentScreen(this, currentUser);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showNewApptScreen(UserModel currentUser) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/EditAppointmentPage.fxml"));
            AnchorPane newApptScreen = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("New Appointment");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(newApptScreen);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            EditAppointmentController controller = loader.getController();
            controller.setDialogStage(dialogStage, currentUser);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showEditApptScreen(AppointmentModel appointment, UserModel currentUser) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/EditAppointmentPage.fxml"));
            AnchorPane editApptScreen = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Appointment");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(editApptScreen);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            EditAppointmentController controller = loader.getController();
            controller.setDialogStage(dialogStage, currentUser);
            controller.setAppointment(appointment);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void showCustomerScreen(UserModel currentUser) {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/CustomerPage.fxml"));
            AnchorPane customerScreen = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            menu.setCenter(customerScreen);

            // Give the controller access to the main app.
            CustomerPageController controller = loader.getController();
            controller.setCustomerScreen(this, currentUser);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showReports(UserModel currentUser) {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/View/ReportsPage.fxml"));
            TabPane tabPane = (TabPane) loader.load();

            // Set person overview into the center of root layout.
            menu.setCenter(tabPane);

            // Give the controller access to the main app.
            ReportsPageController controller = loader.getController();
            controller.setReports(this, currentUser);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


