package controllers;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.*;


public class IC_addPartController implements Initializable {
    private String errmsg = "";
    private boolean isOutsourced;
    private int ID;

    @FXML
    private Label MachineIDLabel;
    @FXML
    private TextField IDField;
    @FXML
    private TextField NameField;
    @FXML
    private TextField InvCountField;
    @FXML
    private TextField PriceField;
    @FXML
    private TextField MinField;
    @FXML
    private TextField MachineIDField;
    @FXML
    private TextField MaxField;
    @FXML
    private RadioButton InHouseButton;
    @FXML
    private RadioButton OutsourceButton;

    @FXML
    void InHouseSelectRadioBtn(ActionEvent event) {
        isOutsourced = false;
        InHouseButton.setSelected(true);
        MachineIDLabel.setText("Machine ID");
    }

    @FXML
    void OutsourcedSelectRadioBtn(ActionEvent event) {
        isOutsourced = true;
        OutsourceButton.setSelected(true);
        MachineIDLabel.setText("Company Name");
    }

    @FXML
    void SaveClickedBtn(ActionEvent event) throws IOException {
        String name = NameField.getText();
        String inv = InvCountField.getText();
        String price = PriceField.getText();
        String min = MinField.getText();
        String max = MaxField.getText();
        String machineID = MachineIDField.getText();

        try {

            errmsg = IC_part.ValidatePart(name, Integer.parseInt(min), Integer.parseInt(max), Integer.parseInt(inv), Double.parseDouble(price), errmsg);

            if (errmsg.length() > 0) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Error Adding Part");
                alert.setContentText(errmsg);
                alert.showAndWait();
                errmsg = "";
            }


            else {
                if (!isOutsourced) {
                    IC_inHousePart inHousePart = new IC_inHousePart();
                    inHousePart.setPartID(ID);
                    inHousePart.setName(name);
                    inHousePart.setInStock(Integer.parseInt(inv));
                    inHousePart.setPrice(Double.parseDouble(price));
                    inHousePart.setMin(Integer.parseInt(min));
                    inHousePart.setMax(Integer.parseInt(max));
                    inHousePart.setMachineID(Integer.parseInt(machineID));
                    IC_inventory.addPart(inHousePart);

                } else {
                    IC_outSourcedPart outsourcedPart = new IC_outSourcedPart();
                    outsourcedPart.setPartID(ID);
                    outsourcedPart.setName(name);
                    outsourcedPart.setInStock(Integer.parseInt(inv));
                    outsourcedPart.setPrice(Double.parseDouble(price));
                    outsourcedPart.setMin(Integer.parseInt(min));
                    outsourcedPart.setMax(Integer.parseInt(max));
                    outsourcedPart.setCompanyName(machineID);
                    IC_inventory.addPart(outsourcedPart);
                }

                Parent partsSave = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                Scene scene = new Scene(partsSave);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        } catch (NumberFormatException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Blank fields are not allowed. ");
            alert.showAndWait();
        }
    }




    @FXML
    void CancelClickedBtn(ActionEvent event) {
        Alert cancelAlert = new Alert(AlertType.CONFIRMATION);
        cancelAlert.initModality(Modality.NONE);
        cancelAlert.setTitle("Cancel");
        cancelAlert.setHeaderText("Cancel");
        cancelAlert.setContentText("Cancel and go to main screen?");

        cancelAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                Parent partsCancel = null;
                try {
                    partsCancel = FXMLLoader.load(getClass().getResource("/views/IC_main.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Scene scene = new Scene(partsCancel);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        });


    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ID = IC_inventory.getPartCount();
        IDField.setText(String.valueOf(ID));
        InHouseButton.setSelected(true);
    }



}
