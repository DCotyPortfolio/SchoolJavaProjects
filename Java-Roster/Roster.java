
//Roster Class for Scripting and Programming Applications
//David Coty, Western Governors University
//August 2017

import java.util.ArrayList;

public class Roster {

    //start off by creating a roster arraylist
    private static ArrayList<Student> StudentRoster = new ArrayList<>();
    //main method
    public static void main(String[] args) {
        //put the stuff we need to do here!
        //1. add students - My information is in the last entry.
        add(1, "John", "Smith",   "John1989@gmail.com",     20, 88,79,59);
        add(2, "Suzan","Erickson","Erickson_1990@gmailcom", 19, 91,72,85);
        add(3, "Jack", "Napoli",  "The_lawyer99yahoo.com",  19, 85,84,87);
        add(4, "Erin", "Black",   "Erin.black@comcast.net", 22, 91,98,82);
        add(5, "David","Coty",    "dcoty@my.wgu.edu",       40, 88,90,75);
        //2. print all students

        System.out.println("*************ALL STUDENTS -- MY INFO IS LAST (# 5)**************");
        print_all();
        //3. print average grades - i am printing them individually.

        System.out.println("*************AVERAGE GRADES PER STUDENT*************************");
        print_avg_grade(1);
        print_avg_grade(2);
        print_avg_grade(3);
        print_avg_grade(4);
        print_avg_grade(5);
        //4. print all invalid email addresses in entries. NOTE i am doing this before removing a student.
        //student number 3 has an invalid email address! I will display it first before removing.

        System.out.println("*************INVALID EMAILS - BEFORE RECORD REMOVAL*************");
        print_invalid_emails();
        //4. remove a student - Rubric says to remove student 3.

        System.out.println("*************REMOVING RECORD 3**********************************");
        remove(3);
        //print_all again to see if it has really been removed

        System.out.println("*************PRINT ALL TO VERIFY RECORD REMOVAL*****************");
        print_all();
        //5. remove again(should generate a custom message)

        System.out.println("*************REMOVING RECORD 3 AGAIN. SHOULD SEE MSG************");
        remove(3);
        //print_all one more time to see if it has been removed

        System.out.println("*************PRINT ALL TO VERIFY RECORD REMOVAL*****************");
        print_all();
        //finally, run print_invalid_emails one more time to show the difference on before and after
        //purging the record.

        System.out.println("*************INVALID EMAILS - AFTER RECORD REMOVAL*************");
        print_invalid_emails();

        System.out.println("*************LIVE LONG AND PROSPER******************************");


    }
    public static void add(int StudentID, String FirstName, String LastName, String EmailAddress, int Age, int grade1, int grade2, int grade3)
    {
        //keeping Student ID an int as specified in the Student class. Creating an array to house the grades.
        int[] grades = new int[]{grade1, grade2, grade3};
        Student newStudent = new Student(StudentID, FirstName, LastName, EmailAddress, Age, grades);
        StudentRoster.add(newStudent);
    }


    public static void remove(int StudentID) {
        //the rubric says this needs to be a string, but there are several problems with doing this related
        //to Java and some of the version 8 new features. So i kept StudentID an int and things are
        //a lot more stable. This code removes an entry and will show a message when the Student ID does not exist.

            for (int i = 0; i < StudentRoster.size(); i++) {
                if (StudentRoster.get(i).getStudentID() == StudentID) {
                    StudentRoster.remove(i);
                    System.out.println("\nRecord found for Student ID " + StudentID + ". It has been removed.");
                    return;
                } else {
                    i++;
                }
            }
        System.out.println("\nRecord for Student ID " + StudentID + " has already been removed or does not exist!");
        }


public static void print_invalid_emails() {
    //make a string called email address. loop the array list getEmailAddress function into it.
    // if @ or . is not present in the string, it's invalid - regardless of location in the string.
    //NOTE: call this BEFORE you call remove(). Otherwise we don't see all the invalid email addresses,
    //because one of them is removed.
    String EmailAddress;
    for (int i = 0; i < StudentRoster.size(); i++) {
        EmailAddress = StudentRoster.get(i).getEmailAddress();
           if (!EmailAddress.contains("@") || !EmailAddress.contains(".") || !EmailAddress.matches("\\S+")) {
            //email address is invalid!
            System.out.println(EmailAddress);
        }
    }
}


    public static void print_avg_grade(int StudentID) {
        //keeping Student ID an int. This is where things get really buggy if you make something meant to
        //be an int a string. Math is a lot easier when you use numbers. averaging as we do. Add the entries
        //and divide by the number of entries.

        for (Student s : StudentRoster) {
            if (s.getStudentID() == StudentID){
                int average = (s.getGrades()[0] + s.getGrades()[1] + s.getGrades()[2]) / 3;
                System.out.println("\nAverage Grade for " + s.getFirstName() + " " + s.getLastName() + " is " +
                        average);
            }
        }
    }

    public static void print_all(){
        //this just prints everything tabbed-up. Print is defined int the student class.

        System.out.println("\nStudent Roster");
        for(int i=0; i < StudentRoster.size(); i++){
            StudentRoster.get(i).print();
        }
    }


    }

