package models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

//created on 3/18/18 by David Coty
public class IC_outSourcedPart extends IC_part {
    private final StringProperty companyName;

    public IC_outSourcedPart() {
        super();
        companyName = new SimpleStringProperty();
    }

    public void setCompanyName(String companyName) {
        this.companyName.set(companyName);
    }

    public String getCompanyName() {
        return this.companyName.get();
    }
}
