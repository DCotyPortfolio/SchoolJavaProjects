package models;

import controllers.IC_modifyProductController;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

//created on 3/18/18 by David Coty


public class IC_product {
    //Instance variables
    private static ObservableList<IC_part> associatedParts = FXCollections.observableArrayList();
    private final IntegerProperty productID;
    private final StringProperty name;
    private final DoubleProperty price;
    private final IntegerProperty inStock;
    private final IntegerProperty min;
    private final IntegerProperty max;

    //Constructor
    public IC_product() {
        productID = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        price = new SimpleDoubleProperty();
        inStock = new SimpleIntegerProperty();
        min = new SimpleIntegerProperty();
        max = new SimpleIntegerProperty();
    }

    //Getters
    public IntegerProperty ID() {
        return productID;
    }

    public StringProperty name() {
        return name;
    }

    public DoubleProperty price() {
        return price;
    }

    public IntegerProperty Inv() {
        return inStock;
    }

    public int getProductID() {
        return this.productID.get();
    }

    public String getName() {
        return this.name.get();
    }

    public double getPrice() {
        return this.price.get();
    }

    public int getInStock() {
        return this.inStock.get();
    }

    public int getMin() {
        return this.min.get();
    }

    public int getMax() {
        return this.max.get();
    }

    public ObservableList getParts() {
        return associatedParts;
    }

    //Setters
    public void setProductID(int productID) {
        this.productID.set(productID);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public void setInStock(int inStock) {
        this.inStock.set(inStock);
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public void setMax(int max) {
        this.max.set(max);
    }

    public void setParts(ObservableList<IC_part> parts) {
        IC_product.associatedParts = parts;
    }


    public static String isProductValid(String name, int min, int max, int inv, double price, ObservableList<IC_part> parts, String message) {
        double sumParts = 0.00;
        for (IC_part part : parts) {
            sumParts = sumParts + part.getPrice();
        }
        if (name.equals("")) {
            message = message + ("Name field is blank.");
        }
        if (min < 0) {
            message = message + ("The inventory must be greater than 0.");
        }
        if (price < 0) {
            message = message + ("The price must be greater than $0");
        }
        if (min > max) {
            message = message + ("The inventory MIN must be less than the MAX.");
        }
        if (inv < min || inv > max) {
            message = message + ("Part inventory must be between MIN and MAX values.");
        }
        if (parts.size() < 1) {
            message = message + ("Product must contain at least 1 part.");
        }

        if (sumParts > price) {
            message = message + ("Product price must be greater than cost of associatedParts.");
        }
        return message;
    }
}
